#! /bin/bash

#-------------------- beginning-------------------

version=$(cat /etc/issue)

#checks ubuntu version and installs dependencies accordingly installs dependencies
	
	if [ -z "${version##*"18.04"*}" ] #Its a mess but dash doesn't do double brackets
        	then sudo apt-get -y install lib32gcc1 lib32stdc++6 libc6-i386 libusb-0.1-4
		echo "installed dependencies for 18.04"
	else sudo apt-get install lib32stdc++6 ia32-libs
		echo "installed older dependencies"
	fi

#gets packages

	wget -T 10 -nd --no-cache http://www.brother.com/pub/bsc/linux/packages/mfc9970cdwlpr-1.1.1-5.i386.deb
	wget -T 10 -nd --no-cache http://www.brother.com/pub/bsc/linux/packages/mfc9970cdwcupswrapper-1.1.1-5.i386.deb

#depackages them

	sudo dpkg -i --force-all mfc9970cdwlpr-1.1.1-5.i386.deb
	sudo dpkg -i --force-all mfc9970cdwcupswrapper-1.1.1-5.i386.deb

#gets scan packages and depackages them

	wget -T 10 -nd --no-cache http://www.brother.com/pub/bsc/linux/packages/brscan4-0.4.6-1.amd64.deb
	sudo dpkg -i --force-all brscan4-0.4.6-1.amd64.deb
	wget -T 10 -nd --no-cache http://www.brother.com/pub/bsc/linux/packages/brscan-skey-0.2.4-1.amd64.deb
	sudo dpkg -i --force-all brscan-skey-0.2.4-1.amd64.deb

#restart cups service

	sudo systemctl restart cups.service

#configure printer parameters and ip address

	sudo lpadmin -p 'MFC9970CDW' -v socket://192.168.20.51 -E





