#!/bin/bash
sudo useradd guest-prefs -m -g sudo -p '$6$wR/fwuO2$9lO4aPCSW1DQgiKTcjPcGv8R5V5523hfHjuXhmWEiQnlKYDnGri.kyOa0V3XA8pZNikwFwuy.4p3v5x6/whbx/'
sudo ln -s /home/guest-prefs /etc/guest-session/skel
sudo -i -u guest-prefs bash << EOF
echo in
mkdir -p /home/guest-prefs/.config/upstart
echo "start on starting unity7" > /home/guest-prefs/.config/upstart/lowgfx.conf
echo "pre-start script" >> /home/guest-prefs/.config/upstart/lowgfx.conf
echo "initctl set-env -g UNITY_LOW_GFX_MODE=1" >> /home/guest-prefs/.config/ups$
echo "end script" >> /home/guest-prefs/.config/upstart/lowgfx.conf
EOF
echo out
